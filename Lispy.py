# coding = utf-8
"""A Lisp interpreter using Python."""
from __future__ import division

import math
import operator

Symbol = str
Number = (int, float)
List = list


###############################################################################


class Procedure(object):
    """Class for user defined Procedure."""

    def __init__(self, params, body, env):
        """Construct function."""
        self.params, self.body, self.env = params, body, env

    def __call__(self, *args):
        """Function call."""
        return evaluate(self.body, Env(self.params, args, self.env))


class Env(dict):
    """Class for environment.

    An environment is a dictionary, {'var': value}
    """

    def __init__(self, params=(), args=(), father_env=None):
        """Initial function, set current environment and inherit fatherenv."""
        self.father_env = father_env
        self.update(zip(params, args))

    def find(self, var):
        """Find var wether in current environment or father environment.

        :param var:
        """
        return self if (var in self) else self.father_env.find(var)

###############################################################################


def set_env():
    """Set interpreter running environment."""
    env = Env()
    env.update(vars(math))
    [env.pop(_) for _ in ['__doc__', '__file__', '__name__', '__package__']]
    env.update({
        '+': (lambda *args: reduce(operator.add, args)),
        '-': (lambda *args: reduce(operator.sub, args)),
        '*': (lambda *args: reduce(operator.mul, args)),
        '/': (lambda *args: reduce(operator.div, args)),
        '>': operator.gt,
        '<': operator.lt,
        '>=': operator.ge,
        '<=': operator.le,
        '=': operator.eq,
        'abs': abs,
        'append': operator.add,
        'apply': apply,
        'begin': (lambda *x: x[-1]),
        'car': (lambda x: x[0]),
        'cdr': (lambda x: x[1:]),
        'cons': (lambda x, y: [x] + y),
        'eq?': operator.is_,
        'equal?': operator.eq,
        'length': len,
        'list': (lambda *x: list(x)),
        'list?': (lambda x: isinstance(x, list)),
        'map': map,
        'max': max,
        'min': min,
        'not': operator.not_,
        'null?': (lambda x: x == []),
        'number?': (lambda x: isinstance(x, Number)),
        'procedure?': callable,
        'round': round,
        'symbol?': (lambda x: isinstance(x, Symbol)),
    })
    return env


# global global_env
global_env = set_env()


###############################################################################


def tokenize_from(input_string):
    """Split string to single object.

    :param input_string:
    """
    return input_string.replace('(', ' ( ').replace(')', ' ) ').split()


def real_meaning(atom):
    """Every atom object has its real meaing.

    :param atom:
    """
    try:
        return int(atom)
    except ValueError:
        try:
            return float(atom)
        except ValueError:
            return Symbol(atom)


def read_from(tokens):
    """Arrange tokens to a tree.

    :param tokens:
    """
    if len(tokens) == 0:
        # raise SyntaxError('Unexpected EOF')
        return
    token = tokens.pop(0)
    if token == '(':
        tree_list = []
        while tokens[0] != ')':
            tree_list.append(read_from(tokens))
        tokens.pop(0)
        return tree_list
    elif token == ')':
        raise SyntaxError('Unexpected ")"')
    else:
        return real_meaning(token)


def parser(program):
    """Parser program to a list.

    :param program:
    """
    return read_from(tokenize_from(program))


###############################################################################


def evaluate(input_exp, env=global_env):
    """Evaluate the input_exp.

    :param env:
    :param input_exp:
    """
    if isinstance(input_exp, Symbol):  # variable reference ===>var
        return env.find(input_exp)[input_exp]
    elif not isinstance(input_exp, List):  # constant literal ===> number
        return input_exp
    elif input_exp[0] == 'quote':  # quotation ===> (quote input_exp)
        (_, output_exp) = input_exp
        return output_exp
    elif input_exp[0] == 'if':  # conditional===> (if test conseq other)
        (_, test, conseq, other) = input_exp
        output_exp = (conseq if evaluate(test, env) else other)
        return evaluate(output_exp, env)
    elif input_exp[0] == 'define':  # definition ===> (define var exp)
        (_, var, output_exp) = input_exp
        env[var] = evaluate(output_exp, env)  # Store var into env
    elif input_exp[0] == 'set!':     # assignment ===> (set! var exp)
        (_, var, exp) = input_exp
        env.find(var)[var] = evaluate(exp, env)
    elif input_exp[0] == 'lambda':
        (_, params, body) = input_exp
        return Procedure(params, body, env)
    else:  # procedure call ===> (proc arg...)
        proc = evaluate(input_exp[0], env)
        args = [evaluate(arg, env) for arg in input_exp[1:]]
        return proc(*args)


###############################################################################


def lisp_str(expression):
    """Convert expression to lisp style.

    :param expression:
    """
    if isinstance(expression, list):
        return '(' + ' '.join(map(lisp_str, expression)) + ')'
    else:
        return str(expression)


def repl(prompt='Lispy.py> '):
    """A repl.

    :param prompt:
    """
    while True:
        try:
            result = evaluate(parser(raw_input(prompt)))
            if result is not None:
                print lisp_str(result)
        except EOFError:
            print "\nGoodBye!\n"
            return


if __name__ == '__main__':
    repl()
